#import <AFNetworking/AFNetworking.h>

@class CPLWorkAssignment;

@interface CPLWorkAssignmentsLoader : NSObject

@property (nonatomic, strong, readonly) AFHTTPSessionManager *sessionManager;

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithSessionManager:(AFHTTPSessionManager *)sessionManager NS_DESIGNATED_INITIALIZER;

- (void)loadWorkAssignmentsWithSuccess:(void(^)(NSArray<CPLWorkAssignment *> *))success
                               failure:(void(^)(NSError *))failure;

@end
