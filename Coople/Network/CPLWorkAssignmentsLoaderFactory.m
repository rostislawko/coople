#import <AFNetworking/AFNetworking.h>
#import "CPLWorkAssignmentsLoaderFactory.h"
#import "CPLWorkAssignmentsLoader.h"

@implementation CPLWorkAssignmentsLoaderFactory

- (CPLWorkAssignmentsLoader *)createWorkAssignmentsLoader {
    return [[CPLWorkAssignmentsLoader alloc] initWithSessionManager:[AFHTTPSessionManager new]];
}

@end
