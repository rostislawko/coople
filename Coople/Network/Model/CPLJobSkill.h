#import <Mantle/Mantle.h>

@interface CPLJobSkill : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *jobProfileId;
@property (nonatomic, copy, readonly) NSNumber *educationalLevelId;

@end
