#import "CPLWage.h"

@implementation CPLWage

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"amount" : @"amount",
        @"currencyId" : @"currencyId"
    };
}

@end
