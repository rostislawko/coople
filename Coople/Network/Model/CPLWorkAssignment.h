#import <Mantle/Mantle.h>
#import "CPLJobLocation.h"
#import "CPLJobSkill.h"
#import "CPLWage.h"

@interface CPLWorkAssignment : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSUUID *workAssignmentId;
@property (nonatomic, copy, readonly) NSString *workAssignmentName;
@property (nonatomic, copy, readonly) NSString *workAssignmentReadableId;

@property (nonatomic, strong, readonly) CPLJobLocation *jobLocation;
@property (nonatomic, strong, readonly) CPLJobSkill *jobSkill;
@property (nonatomic, strong, readonly) CPLWage *hourlyWage;
@property (nonatomic, strong, readonly) CPLWage *salary;
@property (nonatomic, strong, readonly) CPLWage *hourlyWageWithHolidayPay;
@property (nonatomic, strong, readonly) CPLWage *salaryWithHolidayPay;

@property (nonatomic, copy, readonly) NSNumber *periodFrom;

@end
