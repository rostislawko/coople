#import "CPLWorkAssignmentsDownloadResult.h"

@implementation CPLWorkAssignmentsDownloadResult

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"status" : @"status",
        @"workAssignments" : @"data.items",
        @"errorCode" : @"errorCode",
        @"errorDetails" : @"errorDetails",
        @"hasError" : @"error",
    };
}

+ (NSValueTransformer *)hasErrorJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)workAssignmentsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:CPLWorkAssignment.class];
}

@end
