#import <Mantle/Mantle.h>
#import "CPLWorkAssignment.h"

@interface CPLWorkAssignmentsDownloadResult : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *status;
@property (nonatomic, copy, readonly) NSArray<CPLWorkAssignment *> *workAssignments;
@property (nonatomic, copy, readonly) NSString *errorCode;
@property (nonatomic, copy, readonly) NSDictionary *errorDetails;
@property (nonatomic, assign, readonly) BOOL hasError;

@end
