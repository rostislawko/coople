#import <Mantle/Mantle.h>

@interface CPLJobLocation : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSString *addressStreet;
@property (nonatomic, copy, readonly) NSString *extraAdress;
@property (nonatomic, copy, readonly) NSString *zip;
@property (nonatomic, copy, readonly) NSString *city;
@property (nonatomic, copy, readonly) NSString *state;
@property (nonatomic, copy, readonly) NSNumber *countryId;

@end
