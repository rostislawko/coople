#import "CPLJobLocation.h"

@implementation CPLJobLocation

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"addressStreet" : @"addressStreet",
        @"extraAdress" : @"extraAdress",
        @"zip" : @"zip",
        @"city" : @"city",
        @"state" : @"state",
        @"countryId" : @"countryId"
    };
}

@end
