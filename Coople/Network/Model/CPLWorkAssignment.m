#import "CPLWorkAssignment.h"

@implementation CPLWorkAssignment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
  return @{
        @"workAssignmentId" : @"workAssignmentId",
        @"workAssignmentName" : @"workAssignmentName",
        @"workAssignmentReadableId" : @"waReadableId",
        @"jobLocation" : @"jobLocation",
        @"jobSkill" : @"jobSkill",
        @"hourlyWage" : @"hourlyWage",
        @"salary" : @"salary",
        @"hourlyWageWithHolidayPay" : @"hourlyWageWithHolidayPay",
        @"salaryWithHolidayPay" : @"salaryWithHolidayPay",
        @"periodFrom" : @"periodFrom",
    };
}

+ (NSValueTransformer *)workAssignmentIdJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLUUIDValueTransformerName];
}

+ (NSValueTransformer *)jobLocationJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CPLJobLocation.class];
}

+ (NSValueTransformer *)jobSkillJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CPLJobSkill.class];
}

+ (NSValueTransformer *)hourlyWageJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CPLWage.class];
}

+ (NSValueTransformer *)salaryJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CPLWage.class];
}

+ (NSValueTransformer *)hourlyWageWithHolidayPayJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CPLWage.class];
}

+ (NSValueTransformer *)salaryWithHolidayPayJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:CPLWage.class];
}

@end
