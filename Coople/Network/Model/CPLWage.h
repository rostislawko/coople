#import <Mantle/Mantle.h>

@interface CPLWage : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly) NSNumber *amount;
@property (nonatomic, copy, readonly) NSNumber *currencyId;

@end
