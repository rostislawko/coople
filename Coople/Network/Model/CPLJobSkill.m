#import "CPLJobSkill.h"

@implementation CPLJobSkill

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"jobProfileId" : @"jobProfileId",
        @"educationalLevelId" : @"educationalLevelId"
    };
}

@end
