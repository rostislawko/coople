#import <Foundation/Foundation.h>

@class CPLWorkAssignment;

@interface CPLDiskCache : NSObject

+ (instancetype)sharedCache;

- (void)cacheWorkAssignments:(NSArray<CPLWorkAssignment *> *)workAssignments;
- (NSArray<CPLWorkAssignment *> *)cachedWorkAssignments;

@end
