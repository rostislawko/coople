#import "CPLDiskCache.h"
#import "CPLWorkAssignment.h"

@implementation CPLDiskCache

- (void)cacheWorkAssignments:(NSArray<CPLWorkAssignment *> *)workAssignments {
    NSString *fileName = [[self class] filePathOnDisk];
    BOOL cachingSucceeded = [NSKeyedArchiver archiveRootObject:workAssignments toFile:fileName];

    if (cachingSucceeded == NO) {
        NSLog(@"%@: Error saving work assignments", [self class]);
    }
}

- (NSArray<CPLWorkAssignment *> *)cachedWorkAssignments
{
    NSString *fileName = [[self class] filePathOnDisk];
    NSArray<CPLWorkAssignment *> *workAssignments = [NSKeyedUnarchiver unarchiveObjectWithFile:fileName];
    if (workAssignments == nil || workAssignments.count == 0) {
        NSLog(@"%@: Cached work assignments not found", [self class]);
    }
    return workAssignments;
}

+ (NSString *)filePathOnDisk
{
    NSArray *cacheDirectories = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = [cacheDirectories objectAtIndex:0];
    return [cacheDirectory stringByAppendingPathComponent:@"WorkAsignments"];
}

+ (instancetype)sharedCache
{
    static CPLDiskCache *defaultCache = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        defaultCache = [[CPLDiskCache alloc] init];
    });
    return defaultCache;
}

@end
