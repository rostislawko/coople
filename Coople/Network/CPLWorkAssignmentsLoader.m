#import <AFNetworkActivityIndicatorManager.h>
#import <Mantle/Mantle.h>

#import "CPLWorkAssignmentsLoader.h"
#import "CPLWorkAssignmentsDownloadResult.h"
#import "CPLWorkAssignment.h"
#import "CPLDiskCache.h"

static NSString *const kCPLDownloadUrl = @"https://www.coople.com/resources/api/work-assignments/public-jobs/list?pageNum=0&pageSize=200";

@interface CPLWorkAssignmentsLoader ()

@property (nonatomic, strong, readwrite) AFHTTPSessionManager *sessionManager;

@end

@implementation CPLWorkAssignmentsLoader

- (instancetype)initWithSessionManager:(AFHTTPSessionManager *)sessionManager {
    self = [super init];
    if (self != nil) {
        _sessionManager = sessionManager;
    }
    return self;
}

- (void)loadWorkAssignmentsWithSuccess:(void(^)(NSArray<CPLWorkAssignment *> *))success
                               failure:(void(^)(NSError *))failure {
    NSParameterAssert(success);
    NSParameterAssert(failure);
    
    NSArray<CPLWorkAssignment *> *workAssignments = [[CPLDiskCache sharedCache] cachedWorkAssignments];
    if (workAssignments != nil && workAssignments.count > 0) {
        if (success != nil) {
            success(workAssignments);
        }
    }
    
    void (^sucessBlock)(NSURLSessionDataTask *, id) = ^void(NSURLSessionDataTask *task, id responseObject) {
        NSError *error = nil;
        CPLWorkAssignmentsDownloadResult *result =
            [MTLJSONAdapter modelOfClass:CPLWorkAssignmentsDownloadResult.class
                      fromJSONDictionary:responseObject
                                   error:&error];
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        if (error != nil) {
            if (failure != nil) {
                failure(error);
            }
        } else if (success != nil) {
            success(result.workAssignments);
            dispatch_async(dispatch_queue_create(DISPATCH_QUEUE_SERIAL, 0), ^{
                [[CPLDiskCache sharedCache] cacheWorkAssignments:result.workAssignments];
            });
        }
    };
    
    void (^failureBlock)(NSURLSessionDataTask *, NSError *) = ^(NSURLSessionDataTask * task, NSError *error) {
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
        if (failure != nil) {
            failure(error);
        }
    };
    
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [self.sessionManager GET:kCPLDownloadUrl
                  parameters:nil
                    progress:nil
                     success:sucessBlock
                     failure:failureBlock];
}

@end
