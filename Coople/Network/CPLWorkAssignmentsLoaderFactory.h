#import <Foundation/Foundation.h>

@class CPLWorkAssignmentsLoader;

@interface CPLWorkAssignmentsLoaderFactory : NSObject

- (CPLWorkAssignmentsLoader *)createWorkAssignmentsLoader;

@end
