#import <AFNetworkActivityIndicatorManager.h>

#import "CPLAppDelegate.h"

@implementation CPLAppDelegate

- (void)applicationDidFinishLaunching:(UIApplication *)application {
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
}

@end
