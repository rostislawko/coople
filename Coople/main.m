#import <UIKit/UIKit.h>
#import "CPLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CPLAppDelegate class]));
    }
}
