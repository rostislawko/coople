#import <UIKit/UIKit.h>

@interface CPLWorkAssignmentTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *workAssignmentNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *adressStreetLabel;
@property (nonatomic, weak) IBOutlet UILabel *zipLabel;
@property (nonatomic, weak) IBOutlet UILabel *cityLabel;

@end
