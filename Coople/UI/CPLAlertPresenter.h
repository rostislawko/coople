#import <UIKit/UIKit.h>

@interface CPLAlertPresenter : NSObject

+ (void)showAlertWithError:(NSError *)error inViewController:(UIViewController *)viewController;

@end
