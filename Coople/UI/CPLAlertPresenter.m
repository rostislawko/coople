#import "CPLAlertPresenter.h"

@implementation CPLAlertPresenter

+ (void)showAlertWithError:(NSError *)error inViewController:(UIViewController *)viewController {
    UIAlertController *alertController =
    [UIAlertController alertControllerWithTitle:@"Error"
                                        message:error.userInfo[NSLocalizedDescriptionKey]
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction =
        [UIAlertAction actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [viewController dismissViewControllerAnimated:YES
                                                                      completion:nil];
                               }];
    [alertController addAction:okAction];
    [viewController presentViewController:alertController animated:YES completion:nil];
}

@end
