#import "CPLViewController.h"
#import "CPLAlertPresenter.h"
#import "CPLWorkAssignmentsLoader.h"
#import "CPLWorkAssignmentsLoaderFactory.h"
#import "CPLWorkAssignment.h"
#import "CPLWorkAssignmentTableViewCell.h"

static NSString *const kCPLWorkAssignmentCellReuseIdentifier = @"WorkAssignmentCell";

@interface CPLViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIView *placeholder;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;

@property (nonatomic, strong, readonly) CPLWorkAssignmentsLoader *workAssignmentsLoader;
@property (nonatomic, strong) NSArray<CPLWorkAssignment *> *workAssignments;

@end

@implementation CPLViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        CPLWorkAssignmentsLoaderFactory *workAssignmentsFactory =
            [CPLWorkAssignmentsLoaderFactory new];
        
        _workAssignmentsLoader = [workAssignmentsFactory createWorkAssignmentsLoader];
        _workAssignments = [NSMutableArray array];
    }
    return self;
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTableView];
    [self updateWorkAssignments];
}

- (IBAction)retry {
    [self updateWorkAssignments];
    self.retryButton.hidden = YES;
}

#pragma mark - Methods

- (void)configureTableView {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100.f;
}

- (void)updateWorkAssignments {
    __weak typeof(self) weakSelf = self;
    void (^success)(NSArray<CPLWorkAssignment *> *) = ^(NSArray<CPLWorkAssignment *> *workAssignments) {
        [weakSelf addNewWorkAssignments:workAssignments];
    };
    void (^failure)(NSError *) = ^(NSError *error) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [CPLAlertPresenter showAlertWithError:error inViewController:strongSelf];
        strongSelf.retryButton.hidden = NO;
    };
    
    [self.workAssignmentsLoader loadWorkAssignmentsWithSuccess:success failure:failure];
}

- (void)addNewWorkAssignments:(NSArray<CPLWorkAssignment *> *)newWorkAssignments {
    @synchronized(self.workAssignments) {
        self.workAssignments = newWorkAssignments;
    }
    [self.tableView reloadData];
    [self setPlaceholderHidden:YES];
}

- (void)setPlaceholderHidden:(BOOL)hidden {
    if (self.placeholder.hidden == hidden) {
        return;
    }
    
    CGFloat alpha = hidden ? 0.f : 1.f;
    self.placeholder.hidden = NO;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.1f animations:^{
        weakSelf.placeholder.alpha = alpha;
    }];
    self.placeholder.hidden = hidden;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.workAssignments == nil ? 0 : self.workAssignments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CPLWorkAssignmentTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:kCPLWorkAssignmentCellReuseIdentifier
                                        forIndexPath:indexPath];
    [self configureCell:cell withWorkAssignment:self.workAssignments[indexPath.row]];
    return cell;
}

- (void)configureCell:(CPLWorkAssignmentTableViewCell *)cell withWorkAssignment:(CPLWorkAssignment *)workAssignment {
    cell.workAssignmentNameLabel.text = workAssignment.workAssignmentName;
    cell.adressStreetLabel.text = workAssignment.jobLocation.addressStreet;
    cell.zipLabel.text = workAssignment.jobLocation.zip;
    cell.cityLabel.text = workAssignment.jobLocation.city;
}

@end
