#import "CPLWorkAssignmentTableViewCell.h"

@implementation CPLWorkAssignmentTableViewCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        UIView *backgroundView = [[UIView alloc] init];
        backgroundView.backgroundColor = [UIColor colorWithRed:0.882f green:0.f blue:0.305f alpha:0.1f];
        self.selectedBackgroundView = backgroundView;
    }
    return self;
}

@end
